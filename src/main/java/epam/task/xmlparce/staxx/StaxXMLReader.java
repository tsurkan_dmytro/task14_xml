package epam.task.xmlparce.staxx;

import epam.task.xmlparce.XmlMain;
import epam.task.xmlparce.model.BankClient;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

import javax.xml.namespace.QName;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.Attribute;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;

public class StaxXMLReader {

    public void showStax(){
        ClassLoader classLoader = new XmlMain().getClass().getClassLoader();
        File inputFile = new File(classLoader.getResource("banksdata.xml").getFile());

        List<BankClient> empList = parseXML(inputFile);
        for(BankClient emp : empList){
            System.out.println(emp.toString());
        }
    }

    private static List<BankClient> parseXML(File fileName) {
        List<BankClient> empList = new ArrayList<>();
        BankClient client = null;
        XMLInputFactory xmlInputFactory = XMLInputFactory.newInstance();
        try {
            XMLEventReader xmlEventReader = xmlInputFactory.createXMLEventReader(new FileInputStream(fileName));
            while(xmlEventReader.hasNext()){
                XMLEvent xmlEvent = xmlEventReader.nextEvent();
                if (xmlEvent.isStartElement()){
                    StartElement startElement = xmlEvent.asStartElement();
                    if(startElement.getName().getLocalPart().equals("item")){
                        client = new BankClient();
                        //Get the 'id' attribute from Employee element
                        Attribute idAttr = startElement.getAttributeByName(new QName("id"));
                        if(idAttr != null){
                            client.setId(Integer.parseInt(idAttr.getValue()));
                        }
                    }

                    //set the other varibles from xml elements
                    else if(startElement.getName().getLocalPart().equals("Name")){
                        xmlEvent = xmlEventReader.nextEvent();
                        client.setBankName(xmlEvent.asCharacters().getData());
                    }else if(startElement.getName().getLocalPart().equals("Country")){
                        xmlEvent = xmlEventReader.nextEvent();
                        client.setCountry(xmlEvent.asCharacters().getData());
                    }else if(startElement.getName().getLocalPart().equals("Type")){
                        xmlEvent = xmlEventReader.nextEvent();
                        client.setType(xmlEvent.asCharacters().getData());
                    }else if(startElement.getName().getLocalPart().equals("Depositor")){
                        xmlEvent = xmlEventReader.nextEvent();
                        client.setDepositorName(xmlEvent.asCharacters().getData());
                    }else if(startElement.getName().getLocalPart().equals("Accountid")){
                        xmlEvent = xmlEventReader.nextEvent();
                        client.setAccountId(Long.valueOf(xmlEvent.asCharacters().getData()));
                    }else if(startElement.getName().getLocalPart().equals("AmountOnDeposit")){
                        xmlEvent = xmlEventReader.nextEvent();
                        client.setAmountOnDeposit(Integer.valueOf(xmlEvent.asCharacters().getData()));
                    }else if(startElement.getName().getLocalPart().equals("Profitability")){
                        xmlEvent = xmlEventReader.nextEvent();
                        client.setProfitability(xmlEvent.asCharacters().getData());
                    }else if(startElement.getName().getLocalPart().equals("TimeConstraints")){
                        xmlEvent = xmlEventReader.nextEvent();
                        client.setTimeConstraints(xmlEvent.asCharacters().getData());
                    }
                }
                //if Employee end element is reached, add employee object to list
                if(xmlEvent.isEndElement()){
                    EndElement endElement = xmlEvent.asEndElement();
                    if(endElement.getName().getLocalPart().equals("item")){
                        empList.add(client);
                    }
                }
            }
        } catch (FileNotFoundException | XMLStreamException e) {
            e.printStackTrace();
        }
        return empList;
    }
}