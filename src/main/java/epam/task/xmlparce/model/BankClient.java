package epam.task.xmlparce.model;

public class BankClient {

    private int id;

    private String bankName;

    private String country;

    private String type;
    private String depositorName;
    private long accountId;
    private int amountOnDeposit;
    private String profitability;
    private String timeConstraints;
    public BankClient() {
    }
    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getDepositorName() {
        return depositorName;
    }

    public void setDepositorName(String depositorName) {
        this.depositorName = depositorName;
    }

    public Long getAccountId() {
        return accountId;
    }

    public void setAccountId(Long accountId) {
        this.accountId = accountId;
    }

    public Integer getAmountOnDeposit() {
        return amountOnDeposit;
    }

    public void setAmountOnDeposit(Integer amountOnDeposit) {
        this.amountOnDeposit = amountOnDeposit;
    }

    public String getProfitability() {
        return profitability;
    }

    public void setProfitability(String profitability) {
        this.profitability = profitability;
    }

    public String getTimeConstraints() {
        return timeConstraints;
    }

    public void setTimeConstraints(String timeConstraints) {
        this.timeConstraints = timeConstraints;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return " Bank name : " + this.bankName +
               " Country : " + this.country +
               " Type : " + this.type +
               " Depositor : " + this.depositorName +
               " Accountid : " + this.accountId +
               " AmountOnDeposit : " + this.amountOnDeposit +
               " Profitability : " + this.profitability +
               " TimeConstraints : " + this.timeConstraints;
    }
}
