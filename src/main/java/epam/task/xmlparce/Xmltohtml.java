package epam.task.xmlparce;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;

import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

public class Xmltohtml {

    public Xmltohtml() {
        try {
            ClassLoader classLoader = new XmlMain().getClass().getClassLoader();
            File inputFileXsl = new File(classLoader.getResource("banksdata.xsl").getFile());
            File inputFileXml = new File(classLoader.getResource("banksdata.xml").getFile());

            TransformerFactory tFactory = TransformerFactory.newInstance();

            Source xslDoc = new StreamSource(inputFileXsl);
            Source xmlDoc = new StreamSource(inputFileXml);

            String outputFileName = "bankdata.html";
            OutputStream htmlFile = new FileOutputStream(outputFileName);
            Transformer trasform = tFactory.newTransformer(xslDoc);

            trasform.transform(xmlDoc, new StreamResult(htmlFile));

        } catch (Exception e){
            e.printStackTrace();
        }
    }

    public static void main(String[] args) throws Exception {



    }

}