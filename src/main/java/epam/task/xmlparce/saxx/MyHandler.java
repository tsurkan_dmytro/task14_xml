package epam.task.xmlparce.saxx;

import epam.task.xmlparce.model.BankClient;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import java.util.ArrayList;
import java.util.List;

public class MyHandler extends DefaultHandler {

    // List to hold BankCl object
    private List<BankClient> empList = null;
    private BankClient bankClient = null;
    private StringBuilder data = null;

    // getter method for bankCl list

    public List<BankClient> getEmpList() {
        return empList;
    }
    public MyHandler() {    }

    boolean bankName = false;
    boolean bCountry = false;
    boolean bType = false;
    boolean bDepositor = false;
    boolean bAccountid = false;
    boolean bAmountOnDeposit = false;
    boolean bProfitability = false;
    boolean bTimeConstraints = false;

    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {

        if (qName.equalsIgnoreCase("item")) {
            // create a new BankCl and put it in Map
            //String id = attributes.getValue("id");
            String id = "1111111";
            // initialize BankCl object and set id attribute
            bankClient = new BankClient();
            bankClient.setId(Integer.parseInt(id));
            // initialize list
            if (empList == null)
                empList = new ArrayList<>();
        } else if (qName.equalsIgnoreCase("Name")) {
            // set boolean values for fields, will be used in setting BankCl variables
            bankName = true;
        } else if (qName.equalsIgnoreCase("Country")) {
            bCountry = true;
        } else if (qName.equalsIgnoreCase("Type")) {
            bType = true;
        } else if (qName.equalsIgnoreCase("Depositor")) {
            bDepositor = true;
        }else if (qName.equalsIgnoreCase("Accountid")) {
            bAccountid = true;
        }else if (qName.equalsIgnoreCase("AmountOnDeposit")) {
            bAmountOnDeposit = true;
        }else if (qName.equalsIgnoreCase("Profitability")) {
            bProfitability = true;
        }else if (qName.equalsIgnoreCase("TimeConstraints")) {
            bTimeConstraints = true;
        }
        // create the data container
        data = new StringBuilder();
    }

    @Override
    public void endElement(String uri, String localName, String qName) throws SAXException {
        if (bankName) {
            //  set Bank name
            bankClient.setBankName(data.toString());
            bankName = false;
        } else if (bCountry) {
            bankClient.setCountry(data.toString());
            bCountry = false;
        } else if (bType) {
            bankClient.setType(data.toString());
            bType = false;
        } else if (bDepositor) {
            bankClient.setDepositorName(data.toString());
            bDepositor = false;
        }else if (bAccountid) {
            bankClient.setAccountId(Long.valueOf(data.toString()));
            bAccountid = false;
        }else if (bAmountOnDeposit) {
            bankClient.setAmountOnDeposit(Integer.valueOf(data.toString()));
            bAmountOnDeposit = false;
        }else if (bProfitability) {
            bankClient.setProfitability(data.toString());
            bProfitability = false;
        }else if (bTimeConstraints) {
            bankClient.setTimeConstraints(data.toString());
            bTimeConstraints = false;
        }

        if (qName.equalsIgnoreCase("item")) {
            // add Employee object to list
            empList.add(bankClient);
        }
    }

    @Override
    public void characters(char ch[], int start, int length) throws SAXException {
        data.append(new String(ch, start, length));
    }
}