package epam.task.xmlparce.saxx;

import epam.task.xmlparce.XmlMain;
import epam.task.xmlparce.model.BankClient;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.io.File;
import java.io.IOException;
import java.util.List;

public class SaxMain {
    public SaxMain() {
    }

    public void showSax(){
        ClassLoader classLoader = new XmlMain().getClass().getClassLoader();
        File inputFile = new File(classLoader.getResource("banksdata.xml").getFile());

        SAXParserFactory saxParserFactory = SAXParserFactory.newInstance();
        try {
            SAXParser saxParser = saxParserFactory.newSAXParser();
            MyHandler handler = new MyHandler();
            saxParser.parse(inputFile ,handler);
            //Get Employees list
            List<BankClient> clientList = handler.getEmpList();
            //print employee information
            for (BankClient client : clientList)
                System.out.println(client);
        } catch (ParserConfigurationException | IOException | org.xml.sax.SAXException e) {
            e.printStackTrace();
        }
    }
}
