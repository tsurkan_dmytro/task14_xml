package epam.task.xmlparce.domm;

import epam.task.xmlparce.XmlMain;
import epam.task.xmlparce.model.BankClient;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class DomParcer {


    public void parceData(){

        List<BankClient> backClients = new ArrayList<>();

            try {
            ClassLoader classLoader = new XmlMain().getClass().getClassLoader();

            File inputFile = new File(classLoader.getResource("banksdata.xml").getFile());

            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.parse(inputFile);
            doc.getDocumentElement().normalize();
            System.out.println("Root element :" + doc.getDocumentElement().getNodeName());
            NodeList nList = doc.getElementsByTagName("item");
            System.out.println("----------------------------");

            for (int temp = 0; temp < nList.getLength(); temp++) {
                Node nNode = nList.item(temp);
                //System.out.println("\nCurrent Element :" + nNode.getNodeName());
                BankClient client = new BankClient();

                if (nNode.getNodeType() == Node.ELEMENT_NODE) {
                    Element eElement = (Element) nNode;
                    client.setBankName(eElement.getElementsByTagName("Name")
                            .item(0)
                            .getTextContent());
                    client.setCountry(eElement.getElementsByTagName("Country")
                            .item(0)
                            .getTextContent());
                    client.setType(eElement.getElementsByTagName("Type")
                            .item(0)
                            .getTextContent());
                    client.setDepositorName(eElement.getElementsByTagName("Depositor")
                            .item(0)
                            .getTextContent());
                    client.setAccountId(Long.valueOf(eElement.getElementsByTagName("Accountid")
                            .item(0)
                            .getTextContent()));
                    client.setAmountOnDeposit(Integer.valueOf(eElement.getElementsByTagName("AmountOnDeposit")
                            .item(0)
                            .getTextContent()));
                    client.setProfitability(eElement.getElementsByTagName("Profitability")
                            .item(0)
                            .getTextContent());
                    client.setTimeConstraints(eElement.getElementsByTagName("TimeConstraints")
                            .item(0)
                            .getTextContent());
                    backClients.add(client);

                }
            }
        } catch (Exception e) {
            e.printStackTrace();
    }

    for (BankClient client : backClients) {
        System.out.println("Bank name : " + client.getBankName());
        System.out.println("Country : " + client.getCountry());
        System.out.println("Type : " + client.getType());
        System.out.println("Depositor : " + client.getDepositorName());
        System.out.println("Accountid : " + client.getAccountId());
        System.out.println("AmountOnDeposit : " + client.getAmountOnDeposit() + "$" );
        System.out.println("Profitability : " + client.getProfitability());
        System.out.println("TimeConstraints : " + client.getTimeConstraints());
        System.out.println("_____________________");
    }

    }
}
