package epam.task.xmlparce;

import epam.task.xmlparce.domm.DomParcer;
import epam.task.xmlparce.saxx.SaxMain;
import epam.task.xmlparce.staxx.StaxXMLReader;


public class XmlMain {
    public static void main(String[] args) {

        DomParcer domParcer = new DomParcer();
        domParcer.parceData();

        SaxMain saxMain = new SaxMain();
        saxMain.showSax();

        System.out.println("===========================");
        System.out.println("=========StAX==============");
        StaxXMLReader staxXMLReader = new StaxXMLReader();
        staxXMLReader.showStax();

        System.out.println("===========================");
        System.out.println("=========Html created======");
        Xmltohtml xmltohtml = new Xmltohtml();


        }
}