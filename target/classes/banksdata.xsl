<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
    <xsl:template match="/">
        <html xsl:version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
            <body style="font-family:Arial;font-size:12pt;background-color:#621F45">
                <h1 align="center" style="color:white;padding:4px">Bank Clients</h1>
                <table style="width:100%" bgcolor="#BD91C3" border="1" align="center" cellpadding="5">
                    <tr style="background-color:#581C6D;color:white;padding:4px">
                        <th>Name</th>
                        <th>Country</th>
                        <th>Type</th>
                        <th>Depositor</th>
                        <th>Accountid</th>
                        <th>AmountOnDeposit</th>
                        <th>Profitability</th>
                        <th>TimeConstraints</th>
                    </tr>
                    <xsl:for-each select="banks/item">
                        <tr style="background-color:teal;color:white;padding:4px" align="center">
                            <td style="font-style:italic">
                               <xsl:value-of select="Name" />
                            </td>
                            <td>
                                <xsl:value-of select="Country" />
                            </td>
                            <td>
                                <xsl:value-of select="Type" />
                            </td>
                            <td>
                                <xsl:value-of select="Depositor" />
                            </td>
                            <td>
                                <xsl:value-of select="Accountid" />
                            </td>
                            <td>
                                <xsl:value-of select="AmountOnDeposit" />
                            </td>
                            <td>
                                <xsl:value-of select="Profitability" />
                            </td>
                            <td>
                                <xsl:value-of select="TimeConstraints" />
                            </td>
                        </tr>
                    </xsl:for-each>
                </table>
            </body>
        </html>
    </xsl:template>
</xsl:stylesheet>